(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#sideNav'
  });

  // Changing the defaults
  window.sr = ScrollReveal({ reset: true });
  sr.reveal('.text-reveal', {
    duration: 1000,
    distance: '200px',
    scale: 1,
    mobile: false
  });

  function stopScrolling (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
  }

  var $autoscroll = $('.autoscroll');
  if ($autoscroll.length > 0 && $autoscroll[0].offsetTop - window.pageYOffset > 100 && /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) == false) {
    setTimeout(function() {
      $('html, body').animate({
        scrollTop: ($autoscroll.offset().top)
      }, 2500, "easeInOutExpo");
    }, 700);
  }

  $(".portfolio-item").on("touchstart touchend", function(e) {
    $(this).toggleClass('portfolio-item-hover');
  });

})(jQuery); // End of use strict
